#!/bin/bash

export PROXY="$ec2_host:3128"
cat Dockerfile | envsubst '$PROXY' | docker build --pull --rm -t ghc-cabal:7.8.4-1.22.0.0 -
